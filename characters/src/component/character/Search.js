import React from "react";
import { makeStyles } from '@material-ui/core/styles';
import { Field, reduxForm } from 'redux-form';
import SearchIcon from "@material-ui/icons/Search";
import { useDispatch } from "react-redux";
import characterWatcher from '../../store/action/characterAction';

const useStyles = makeStyles((theme) => ({
  root: {
    position: 'relative'
  },
  search : {
    position: 'absolute',
    padding: '0px',
    right: '1px',
    background: 'transparent',
    border: 'none',
    top: '13px',
  },
  field:{
    padding : '10px',
    width : '87%'
  }

}));

const Search = () => {  
  const dispatch = useDispatch();
  const classes = useStyles();

  const handleSubmit = e =>{
    console.log('snvjnvjfnvjkldfnvfnvnjn',e.target.value);
    // e.preventDefault();
    var params = {}
    params.gender = "";
    params.species = "";
    // params.name = ""
    dispatch(characterWatcher(params));
  }

  // const handleSearch = e =>{
  //   console.log(e.target.value);
  //   e.preventDefault();
  //   var params = {}
  //   params.gender = "";
  //   params.species = "";
  //   params.name = ""
  //   dispatch(characterWatcher(params));
  // }

  return (
    <>
    <form className={classes.root} onSubmit={handleSubmit}>
          <Field
            className={classes.field}
            name="note"
            component="input"
            type="text"
            placeholder="Search ex: Rick, Morty"
          />
          <button type="submit" className={classes.search}>
            <SearchIcon/>
          </button>
    </form>
    </>
  );
};


export default reduxForm({
  form: 'Search',
})(Search);
