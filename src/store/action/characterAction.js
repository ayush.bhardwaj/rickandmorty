import { CHARACTER_BASED } from "../../types/index";

const characterWatcher = data => {
  return {
    type: CHARACTER_BASED.FETCH_REQUEST,
    data
  };
};

export default characterWatcher;