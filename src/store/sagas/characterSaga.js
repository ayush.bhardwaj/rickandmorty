import { call,put,takeEvery } from 'redux-saga/effects'
import { CHARACTER_BASED } from '../../types/index';
import { getCall } from '../../utils/apiSignature';

// const nameData = state => state.form.Search.values.note;

function* characterWorker(params){
    console.log('sdfsf');
    // console.log((nameData));
    
    const {response,error} = yield call(getCall,`https://rickandmortyapi.com/api/character/?gender=${params.data.gender}&species=${params.data.species}&name=${params.data.name}`);
    if (response){
        yield put({
            type : CHARACTER_BASED.FETCH_SUCCESS,
            payload : response
        })
    }else if(error){
        yield put({
            type : CHARACTER_BASED.FETCH_FAILURE,
            payload : error
        })
    }
}

export default function* characterWatcher() {
  console.log("inside saga")
    yield takeEvery(CHARACTER_BASED.FETCH_REQUEST, characterWorker);
  }