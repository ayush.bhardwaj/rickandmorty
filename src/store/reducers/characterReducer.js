import { CHARACTER_BASED } from '../../types/index'

const characterReducer = (state = {},action) =>{
    console.log('Character Reducer', action)
    switch (action.type) {
        case CHARACTER_BASED.FETCH_REQUEST:
            return{
                ...state,
                loading : true
            }
        
        case CHARACTER_BASED.FETCH_SUCCESS:
            return {
                ...state,
                loading : false,
                data : action.payload,
                error : ''
            }
        
        case CHARACTER_BASED.FETCH_FAILURE:
            return{
                loading : false,
                data : [],
                error : action.payload
            }
    
        default:
            return state;
    }
} 

export default characterReducer;