import { combineReducers } from 'redux';
import characterReducer from './characterReducer';
import { reducer as formReducer } from 'redux-form';


const rootReducer = combineReducers({
  form : formReducer,
  characterReducer,
});

export default rootReducer;
