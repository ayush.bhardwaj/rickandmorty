import React from 'react'
import Typography from '@material-ui/core/Typography';
import Toolbar from '@material-ui/core/Toolbar';
import Link from '@material-ui/core/Link';

const styles = {
    root: {
      flexGrow: 1,
      padding: '65px 325px 32px',
      textAlign: 'center',
      fontWeight : 'bold',
      fontColor : "#232323"
    },
    appbar: {
      alignItems: 'center',
    },
    toolbar:{
      display: 'block'
    },
    charactersLink : {
      textAlign : 'center',
      color : '#551a8b',
      fontSize : '2rem',
      textDecoration : 'underline'
    }
  };

const preventDefault = (event) => event.preventDefault();

function Home() {
    return (
        <div style={styles.appbar}>
            <Toolbar style={styles.toolbar}>
                    <Typography variant="h3" style={styles.root}>
                        THE RICK AND MORTY API, Publicis.Sapient Assignment
                    </Typography>
                    <div style={styles.charactersLink}>
                    <Link href="#" onClick={preventDefault} color="inherit">
                        Click Here to browse all Characters
                    </Link>
                    </div>
                </Toolbar>
        </div>
    )
}

export default Home
