import React, {useRef} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Search from './Search';
import GenderFilter from './GenderFilter';
import Grid from '@material-ui/core/Grid';
import { useDispatch } from "react-redux";
import characterWatcher from '../../store/action/characterAction';

const useStyles = makeStyles((theme) => ({
    root : {
        padding: '12px 30px 12px 12px'
    },
    clear: {
        margin: '17px 2px 0px 2px'
    }
  }));

function Sidebar() {
    const resetHandlerRef = useRef();
    const dispatch = useDispatch();
    const classes = useStyles();
    const handleClear = () =>{
        resetHandlerRef.current.ref.current.reset();
        var params = {}
        params.gender = "";
        params.species = "";
        params.name = "";
        dispatch(characterWatcher(params));
      }

    return (
        <div className={classes.root}>
            <Search/> 
            <div>
                <Grid container spacing={1}>
                    <Grid container item xs={12} spacing={3}>
                        <Grid item xs={6}>
                            <h2>Filters</h2>
                        </Grid>
                        <Grid item xs={6}>
                            <div className={classes.clear}><button onClick={handleClear}>Clear All</button></div>
                        </Grid>
                    </Grid>                
                </Grid>
                <GenderFilter ref={resetHandlerRef}/> 
            </div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
        </div>
    )
}

export default Sidebar
