import React, {useState} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Switch from '@material-ui/core/Switch';
import Grid from '@material-ui/core/Grid';
import Sidebar from './Sidebar';
import LayoutCard from './LayoutCard';
import { useSelector } from "react-redux";


const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
      padding: '24px 0px 10px 30px',
      background: 'rgb(32, 35, 41)',
      margin: '20px 40px',
      color: 'white',
    },
    paper: {
      padding: theme.spacing(1),
      textAlign: 'center',
      color: theme.palette.text.secondary,
    },
    order : {
        float : 'inline-end'
    }
  }));

function Character() {
    var getAllCharacters = useSelector(state => state.characterReducer.data && state.characterReducer.data.data.results);
    const [order, setOrder] = useState(true);
    const classes = useStyles();
    const handleOrderChange = () =>{
        setOrder(!order);
        getAllCharacters = Array.prototype.reverse.call(getAllCharacters);
    }
    return (
        <div className={classes.root}>
            <Grid container spacing={1}>
                <Grid container item xs={12} spacing={3}>
                    <Grid item xs={3}>
                        <Sidebar/>
                    </Grid>
                    <Grid item xs={9}>
                        <div className={classes.order}>
                            <Switch
                                checked={order}
                                onChange={handleOrderChange}
                                name="order"
                                inputProps={{ 'aria-label': 'secondary checkbox' }}
                            />{order ? <span>Ascending</span> : <span>Descending</span>}
                        </div>
                            <Grid container item xs={12} spacing={1}>
                                {getAllCharacters && getAllCharacters.map(item => {
                                    const name = item.name
                                    const status = item.status
                                    const gender = item.gender
                                    const image = item.image
                                    const species = item.species
                                    const id = item.id
                                    const location = item.location.name
                                    const origin = item.origin.name
                                    return (
                                        <Grid item key={id} xs={3}>
                                            <LayoutCard 
                                            name={name} 
                                            status={status} 
                                            gender={gender} 
                                            image={image} 
                                            species={species}
                                            id = {id}
                                            location={location}
                                            origin={origin} />
                                        </Grid>
                                    )
                                })}                                         
                        </Grid>
                    </Grid>
                </Grid>                
            </Grid>
        </div>
    )
}

export default Character
