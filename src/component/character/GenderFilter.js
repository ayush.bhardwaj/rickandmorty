import React, { useState, useEffect } from "react";
import { makeStyles } from '@material-ui/core/styles';
import { Field, reduxForm } from 'redux-form';
import { useDispatch } from "react-redux";
import characterWatcher from '../../store/action/characterAction';

const useStyles = makeStyles((theme) => ({
  filter: {
    lineHeight: '2',
    border: '1px solid white',
    padding: '5px 1px 9px 13px',
    margin: '22px auto',
  },
  title:{
    fontSize: '1.25rem'
  }
}));

const GenderFilter = React.forwardRef((props,ref) => { 
  const [gender, setGender] = useState("");
  const [species, setSpecies] = useState("");
  const dispatch = useDispatch();

  useEffect(() => {
    var params = {}
    params.gender = gender;
    params.species = species;
    params.name = "";
    dispatch(characterWatcher(params));
  }, [dispatch,gender,species]);

  const classes = useStyles();

  const handleGenderChange = e =>{
    setGender(e.target.value);
  }

  const handleSpeciesChange = e =>{
    setSpecies(e.target.value);
  }

  return (
    <>
    <form ref={ref}>
      <div className={classes.filter}>
      <label className={classes.title}>Gender</label>
        <div>
          <label><Field name="gender" onChange={e => handleGenderChange(e)} component="input" type="radio" value="female"/> female</label><br/>
          <label><Field name="gender" onChange={e => handleGenderChange(e)} component="input" type="radio" value="male"/> male</label><br/>
          <label><Field name="gender" onChange={e => handleGenderChange(e)} component="input" type="radio" value="genderless"/> genderless</label><br/>
          <label><Field name="gender" onChange={e => handleGenderChange(e)} component="input" type="radio" value="unknown"/> unknown</label>
        </div>
      </div>
      <div className={classes.filter}>
      <label className={classes.title}>Species</label>
        <div>
          <label><Field name="species" onChange={e => handleSpeciesChange(e)} component="input" type="radio" value="human"/> human</label><br/>
          <label><Field name="species" onChange={e => handleSpeciesChange(e)} component="input" type="radio" value="alien"/> alien</label><br/>
          <label><Field name="species" onChange={e => handleSpeciesChange(e)} component="input" type="radio" value="humanoid"/> Humanoid</label>
        </div>
      </div>
    </form>
    </>
  );
});


export default reduxForm({
  form: 'GenderFilter',
})(GenderFilter);
