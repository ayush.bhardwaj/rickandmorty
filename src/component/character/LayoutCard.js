import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import "../../styles/layout.css";

const useStyles = makeStyles({
  root: {
    minWidth: 193.55,
    maxWidth: 193.55,
    borderRadius: "0.625rem"
  },
  bullet: {
    display: "inline-block",
    margin: "0 2px",
    transform: "scale(0.8)"
  },
  title: {
    fontSize: 14
  },
  pos: {
    marginBottom: 12
  },
  media: {
    height: 204,
    width: "100%",
    position : 'relative'
  },
  overlay: {
    // top: "64%",
    left: "0px",
    color: "white",
    position: "absolute",
    backgroundColor: "#000000c2",
    padding: "3px 11px",
    width: "89%",
    bottom : 0
  },
  cardContent: {
    padding: 0,
    paddingBottom: "0px !important"
  },
  cells: {
    padding: "10px 0px",
    fontSize: "0.75rem",
    color: "white",
    borderBottom: '1px solid rgb(0, 0, 0)',
    
  },
  tableBlock: {
    background: "transparent",
    border: "0px !important"
  },
  cellDetails: {
    color: "#ff9800",
    textOverflow: "ellipsis",
    borderBottom: '1px solid rgb(0, 0, 0)'
  },
  table: {
    background: "rgba(0, 0, 0, 0.87)",
    padding: 10
  },
  tableContainer: {
    border: "0px !important",
    borderRadius: "0px !important",
    background: "transparent",
    boxShadow: 'none'
  },
  name:{
    margin : '5px 0px'
  },
  details:{
    margin : '5px 0px'
  },
  // row :{
  //   borderBottom: '1px solid rgb(0, 0, 0)'
  // }
});

export default function LayoutCard(props) {
  // console.log(props)
  // const [name, status, gender, image, species, id, location, origin] = props;
  const name = props.name
  const status = props.status
  const gender = props.gender
  const image = props.image
  const species = props.species
  const id = props.id
  const location = props.location
  const origin = props.origin
  const classes = useStyles();
  return (
    <Card className={classes.root}>
      <CardContent className={classes.cardContent}>
        <CardMedia
          className={classes.media}
          image={image}                                                                                                                                                                                                                                                             
          title="Paella dish"
        >
          <div className={classes.overlay}>
            <h3 className={classes.name}>{name}</h3>
            <p className={classes.details}>id : {id}</p>
          </div>
        </CardMedia>
        <div className={classes.table}>
          <TableContainer component={Paper} className={classes.tableContainer}>
            <Table className={classes.tableBlock} aria-label="simple table">
              <TableBody>
                <TableRow className={classes.row}>
                  <TableCell className={classes.cells}>STATUS</TableCell>
                  <TableCell className={classes.cellDetails} align="right">
                    {status}
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell className={classes.cells}>SPECIES</TableCell>
                  <TableCell className={classes.cellDetails} align="right">
                    {species}
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell className={classes.cells}>GENDER</TableCell>
                  <TableCell className={classes.cellDetails} align="right">
                    {gender}
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell className={classes.cells}>ORIGIN</TableCell>
                  <TableCell className={classes.cellDetails} align="right">
                    {origin}
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell className={classes.cells}>LAST LOCATION</TableCell>
                  <TableCell className={classes.cellDetails} align="right">
                    {location}
                  </TableCell>
                </TableRow>
              </TableBody>
            </Table>
          </TableContainer>
        </div>
      </CardContent>
    </Card>
  );
}
