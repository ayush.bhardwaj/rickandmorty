import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
// import Button from '@material-ui/core/Button';
import Link from '@material-ui/core/Link';
import Box from '@material-ui/core/Box';
// import IconButton from '@material-ui/core/IconButton';
// import MenuIcon from '@material-ui/icons/Menu';

const style = {
    flexGrow: 1
}
const barStyle = {
    backgroundColor : 'rgb(32, 35, 41)'
}

const preventDefault = (event) => event.preventDefault();

const NavBar = () => {
    return (
        <div>
            <AppBar position="static" style={barStyle}>
                <Toolbar>
                    <Typography variant="h5" style={style}>
                        Rick and Morty Characters
                    </Typography>
                    <Link href="/home" onClick={preventDefault} color="inherit">
                        Home
                    </Link>
                    <Box m={1} bgcolor="inherit"/>
                    <Link href="/character" onClick={preventDefault} color="inherit">
                        Characters
                    </Link>
                </Toolbar>
            </AppBar>
        </div>
    )
}

export default NavBar;
