import React from 'react';
import { Router, Route, browserHistory } from 'react-router';
// import logo from './logo.svg';
import './App.css';
import NavBar from './component/NavBar';
import Home from './component/Home';
import Character from './component/character/Character';
import { Provider } from 'react-redux';
import store  from './store/index';

function App() {
  return (
    <Provider store = {store}>
      {/* <div>
        <NavBar/>
        <Home/>
        <Character/>
      </div> */}
      <NavBar/>
      <Router history={browserHistory}>
        <Route path={"home"} component={Home}/>
        <Route path={"characters"} component={Character}/>
      </Router>
    </Provider>
  );
}

export default App;
